module.exports.numberOfDuplicates = function numberOfDuplicates(arr) {
    const result = [];
    for (let i = 0; i < arr.length; i++) {
      const count = arr.slice(0, i + 1).filter(item => item === arr[i]).length;
      result.push(count);
    }
    return result;
  }

module.exports.countObjectStrength = function countObjectStrength(obj) {
    const weights = {
        undefined: 0,
        boolean: 1,
        number: 2,
        string: 3,
        object: 5,
        function: 7,
        bigint: 9,
        symbol: 10
    };

    let strength = 0;

    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            const propertyType = typeof obj[key];
            if (weights.hasOwnProperty(propertyType)) {
                strength += weights[propertyType];
            }
        }
    }

    return strength;
  }
